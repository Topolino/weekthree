#Name: Joey Carberry
#Date: September 15, 2015
#Project: Colors

def decreaseRed(img):
  for pixel in getPixels(img):
    redVal = getRed(pixel) / 2
    setRed(pixel, redVal)
    
file = 'Users/jcarberry2016/Documents/weekThree/space.jpg'
picture = makePicture(file)
show(picture)

decreaseRed(picture)
repaint(picture)