#Name: Joey carberry
#Date: September 14, 2015
#Project: Number Guess
import random

programRun = True
target = random.randint(0,100)


print ('Pick a number between 1 and 100.')
guessValue = 0

while programRun:
  
  guess = int(raw_input('Your guess: '))
  guessValue = guessValue + 1
  
  if (guess == target):
    print ('Congrats, you won a free iPad!')
    programRun = False
  elif (guessValue >= 5):
    programRun = False
    print ('You have run out of guesses.')
  #elif (guessValue >= target - 10):
    #print ('You are 10 or less below.')
  #elif (guessValue <= target + 10):
    #print ('You are 10 or less above.')
  elif (guess > target and guess < 101):
    print ('You\'re too high.')
  elif (guess < target):
    print ('You\'re too low.')
  elif (guess > 100):
    print ('What are you even trying to do?')
    guessValue = guessValue + 1