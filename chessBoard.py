#Name: Joey Carberry
#Date: September 16, 2015
#Chessboard Project

#Variables to be changed later to inputs for width and height
width = 400
height = 400
blockWidth = 50
blockHeight = 50
x = width/8
y = 0
#CountY and CountX are used to stagger the checkerboard 
countY = 0
countX = 0
#Creates the picture and starts the 'widthRun' while loop to true
newPicture = makeEmptyPicture(width, height, white)
widthRun = true

#Starts making rectangles/squares, when false, rectangle building stops
while widthRun: 
  makeRect = addRectFilled(newPicture, x, y, blockWidth, blockHeight, black)
  x += width/4
  #Everytime a square is made, 1 is added to CountY
  countY += 1 
  
  #Once 8 squares are made, the x value is staggered, and a new row is made
  if (countY == width/blockWidth):
    y+= height/8
    countY = 0
    x = 50
    
  #Once 4 squares are made, the x value is staggered, and a new row is made
  elif (countY == width/(blockWidth*2)):  
    y += height/8
    x = 0
    countX += 1
  
  #If the CountX (which is added to every 2 rows)
  elif (countX == width/blockWidth):
    widthRun = false
    repaint(newPicture)
filePath = '/Users/jcarberry2016/Documents/weekThree/chessBoard.png'
writePictureTo(newPicture, filePath)
